**Установка**  
1. sudo apt-get install virtualenv  
2. pip install virtualenvwrapper  
3. mkvirtualenv invite  
4. workon mkvirtualenv  
5. git clone https://bitbucket.org/Kentaken/invite.git  
6. cd invite/  
7. pip install -r REQUIREMENTS  
8. python manage.py migrate  
9. python manage.py createsuperuser (создать супер-пользователя)  
10. python manage.py runserver 8080  
  
**Использование**  
1. Открыть браузер и в адресной строке вбить localhost:8080.  
2. Войти под супер-пользователем.  
3. Вписать email, отправить приглашение нажатием на Submit.  
4. Перейти по ссылке в консоле.  
5. Зарегистрировать нового пользователя.  
  
**Примечания**  
1. В качестве инвайтов используетсяпервичный ключ модельки Invite.  
2. Нельзя дважды выслать приглашение на один и тот же адрес электронной почты.  
