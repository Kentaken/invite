from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('django.contrib.auth.urls')),
    url(
        r'^accounts/login/$',
        auth_views.LoginView.as_view(
            redirect_authenticated_user=True,
        )
    ),
    url(
        r'^accounts/logout/$',
        auth_views.LogoutView.as_view(next_page=r'/'),
        name='logout'
    ),
    url(r'^', include('core.urls')),
]
