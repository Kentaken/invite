# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms

from .models import Invite


class GrantInviteForm(forms.Form):
    """Форма выдачи инвайта."""

    email = forms.EmailField(
        label='Email',
        required=True,
        max_length=256,
    )

    def clean_email(self):
        email = self.cleaned_data['email'].strip()
        if Invite.objects.filter(acceptor_email=email).exists():
            raise forms.ValidationError(
                'On this email invite already sended.')
        return email
