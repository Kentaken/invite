# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.urls import reverse
from django.conf import settings

from . import settings as local_settings
from .models import Invite
from django.core.mail import send_mail as django_send_email


def send_email(email, message):
    """
    Отправка сообщения.
    """
    if settings.DEBUG:
        print '{}\n{}\n{}'.format(email, local_settings.EMAIL_SUBJECT, message)
    else:
        # Тут должен был быть запуск асинхронной задачи для отправки email,
        # но для тестового задания обойдемся таким кодом
        django_send_email(
            local_settings.EMAIL_SUBJECT,
            message,
            local_settings.EMAIL_FROM,
            [email],
            fail_silently=False,
        )


def send_invite(request, email):
    """
    Отправка приглашения.
    """
    invite = Invite.objects.create(
        issuer=request.user,
        acceptor_email=email,
    )
    link = request.build_absolute_uri(
        reverse('signup', kwargs={'invite_id': invite.id})
    )
    message = local_settings.EMAIL_TEMPLATE.format(signup_link=link)
    send_email(email, message)
