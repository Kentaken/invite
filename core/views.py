# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, render_to_response, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login, authenticate

from .forms import GrantInviteForm
from .models import Invite
from .helpers import send_invite


@login_required
def index(request):
    """Главная страница."""
    return render(
        request,
        'index.html',
        context={
            'form': GrantInviteForm(),
        },
    )


@login_required
def invite_granted_successfully(request):
    """Показываем, при успешной выдаче инвайта."""
    return render(
        request,
        'successful.html',
    )


@login_required
def grant_invite(request):
    """Выдаем инвайт."""
    form = GrantInviteForm(request.POST)
    if form.is_valid():
        send_invite(request, form.cleaned_data.get('email'))
        return redirect('invite_granted_successfully')
    return render(
        request,
        'index.html',
        context={
            'form': form,
        },
    )


def signup(request, invite_id):
    """Регистрация по инвайту."""
    def render_error(msg):
        return render_to_response(
            'invalid_invite.html',
            context={'message': msg},
        )

    def render_form(form):
        return render(
            request,
            'registration/signup.html',
            {
                'form': form,
                'invite_id': invite_id,
            }
        )

    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():

            try:
                invite = Invite.objects.get(pk=invite_id)
            except Invite.DoesNotExist:
                return render_error(
                    'Such invite does not exists. '
                    'Verify you link and try again.'
                )

            if invite.accept_datetime is not None:
                return render_error(
                    'You invite already used.'
                )

            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')

            user = authenticate(username=username, password=raw_password)
            login(request, user)

            invite.accept(user)

            return redirect('index')
        else:
            return render_form(form)
    else:
        return render_form(UserCreationForm())
