# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from django.utils import timezone


class Invite(models.Model):
    """
    Инвайт
    """

    issuer = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='issuer_invites',
        on_delete=models.SET_NULL,
        verbose_name='Выдавший инвайт',
        null=True,
        blank=True,
    )

    issue_datetime = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Дата выдачи',
    )

    acceptor_email = models.CharField(
        max_length=256,
        null=True,
        blank=True,
        db_index=True,
    )

    acceptor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='acceptor_invites',
        on_delete=models.SET_NULL,
        verbose_name='Принявший инвайт',
        null=True,
        blank=True,
    )

    accept_datetime = models.DateTimeField(
        verbose_name='Дата использования',
        null=True,
        blank=True,
    )

    @classmethod
    def is_exist(cls, id):
        """
        Проверка на существование инвайта.
        :param id: идентификатор инвайта
        :type id: int
        :return: возвращает True, если инвайт с идентификаторм id был выдан
        :rtype: bool
        """
        return cls.objects.filter(id=id).exists()

    @classmethod
    def is_already_used(cls, id):
        """
        Проверка на использование инвайта.
        :param id: идентификатор инвайта
        :type id: int
        :return: True, если инвайт был использован
        :rtype: bool
        """
        return cls.objects.filter(
            id=id,
            accept_datetime__isnull=False,
        ).exists()

    def accept(self, user):
        """
        Делает инвайт использованным.
        :param user: принявший инвайт пользователь
        """
        self.acceptor = user
        self.accept_datetime = timezone.now()
        self.save()

    class Meta:
        verbose_name = 'Приглашение'
        verbose_name_plural = 'Приглашеня'
