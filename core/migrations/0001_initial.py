# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-21 14:07
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Invite',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('issue_datetime', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0432\u044b\u0434\u0430\u0447\u0438')),
                ('acceptor_email', models.CharField(blank=True, db_index=True, max_length=256, null=True)),
                ('accept_datetime', models.DateTimeField(blank=True, null=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u043d\u0438\u044f')),
                ('acceptor', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='acceptor_invites', to=settings.AUTH_USER_MODEL, verbose_name='\u041f\u0440\u0438\u043d\u044f\u0432\u0448\u0438\u0439 \u0438\u043d\u0432\u0430\u0439\u0442')),
                ('issuer', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='issuer_invites', to=settings.AUTH_USER_MODEL, verbose_name='\u0412\u044b\u0434\u0430\u0432\u0448\u0438\u0439 \u0438\u043d\u0432\u0430\u0439\u0442')),
            ],
            options={
                'verbose_name': '\u041f\u0440\u0438\u0433\u043b\u0430\u0448\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u041f\u0440\u0438\u0433\u043b\u0430\u0448\u0435\u043d\u044f',
            },
        ),
    ]
