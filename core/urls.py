from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^grant-invite/$', views.grant_invite, name='grant-invite'),
    url(r'^invite-granted-successfully/$',
        views.invite_granted_successfully,
        name='invite_granted_successfully'),
    url(r'^signup/(?P<invite_id>[0-9]+)/$', views.signup, name='signup'),
]
